# Documents constitutifs de l'association OSP 

## À propos

La caravane Open Source Publishing se constitue à partir de 2006 autour de la manière par laquelle un groupe peut pratiquer le graphisme de manière professionnelle en utilisant pleinement et uniquement des ressources logicielles et culturelles libres. En 2015, la caravane décide de tenter l'expérience de se constituer en association belge de manière plus formelle. Ce mode de fonctionnement qui ne vise pas l'amassage de capitaux ni de bénéfices est ultra-minoritaire dans le champ du graphisme européen. Il semble intéressant de le proposer à d'autres potentiels groupes, et de rassembler les outils qui permettent de faciliter sa mise en place.

## Contenu

L'objet du présent dépôt est de rendre visible et de permettre une traçabilité et une lecture plus facile des modifications des différents textes qui constituent l'asbl-vzw Open Source Publishing.

À sa constitution le 4 mars 2015, l'association Open source Publishing a décidé :
  * de se constituer en association à la fois francophone et néérlandophone (ASBL-VZW) ce qui implique de traduire dans ces deux langues tous les textes légaux;
  * de traduire en anglais tant que possible ces textes pour permettre leur diffusion de manière plus large;
  * que l'objet social de ses statuts légaux déposé au Moniteur contiennent un manifeste qui puisse être lu de manière formelle le cas échéant lors de toute procédure contradictoire;
  * que le reste de ses statuts s'en tienne au minimum prévu dans la loi belge et fasse largement référence, quand possible dans ladite loi, à une Recette d'ordre intérieur et à un registre des membres, tous deux versionnés séparément et librement.
  * d'y adjoindre un accord de collaboration qui tente de rendre compte comment nous envisageons la collaboration aux personnes et organisations avec lesquelles nous allons commencer une relation.

Jusqu'en avril 2018, ces quatre documents sont élaborés dans des pads. En préparation de l'Assemblée générale du 31 mai 2018, ces textes sont basculés dans le présent dépôt Git afin de rendre mieux visibles et traçables les modifications des différents textes qui constituent l'asbl-vzw Open Source Publishing.

  1. [Les statuts de l'association]()
  2. [La recette d'ordre intérieur]()
  3. [Le registre des membres]()
  4. [l'accord de collaboration]()

## Versionnage

Ce dépôt utilise le logiciel de gestion de versions décentralisé [Git](https://fr.wikipedia.org/wiki/Git). Il possède deux branches.
La première, la **master**, est la version stable: ces textes sont publiés d'une manière officielle et formelle.
La seconde, la **discussion**, est l'endroit des réflexions collectives, où l'on
cherche les bonnes articulations et où différentes versions peuvent être comparées.

En utilisant l'interface de Gitlab, on peut passer d'une branche à l'autre en haut à gauche, avant le breadcrumb et cela pour chaque fichier.  
Pour comparer la version Master et Discussion d'un fichier, utiliser dans le menu à gauche `Repository > Compare`.

En utilisant Git en ligne de commande, pour basculer vers la branche discussion, et donc changer de branche, utiliser

```bash
$ git checkout discussion
```

Pour revenir vers la branche master, utiliser à l'inverse

```bash
$ git checkout master
```

Pour comparer les différentes versions d'un fichier d'une branche à l'autre, utiliser

```bash
$ git diff master..discussion -- myfile.md

```

### Organisation des fichiers

```
├── association-1-statutes
│   ├── association-statutes-fr.md
│   ├── moniteur-osp-2015-03-04-fr.pdf
│   ├── moniteur-osp-2015-03-04-nl.pdf
│   ├── moniteur-osp-2016-10-26-fr.pdf
│   ├── moniteur-osp-2016-10-26-nl.pdf
├── association-2-recipe-of-internal-order
│   ├── association-recipe-of-internal-order-en.md
├── association-3-registry
│   ├── association-registry-en.md
├── association-4-collaboration-agreement
│   ├── ├── association-4-collaboration-agreement-en.md
└── README.md
```
