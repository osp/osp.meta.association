

$(document).ready(function(){
  var group = window.location.hash.substring(1)
  if(group == 'OSP') {
    var urlMd = 'http://pads.osp.kitchen/p/collaboration-agreement/export/txt'
  } else {
    var urlMd = 'http://pads.osp.kitchen/p/collaboration-agreement-' + group + '/export/txt'
  }
  $.get( urlMd, function( data ) {
    $( ".content" ).html(marked(data));
    let h1 = $('hr')
    let compt = 1
    h1.each(function(){
      $(this).nextUntil( "hr" ).wrapAll( "<article id='art" + compt + "' ></article>" )
      $(this).prependTo('#art' + compt)
      compt++
    })

    // let imgs = $('img')
    // imgs.each(function(){
    //   let src = $(this).attr('src')
    //   let clss = $(this).attr('alt')
    //   let ext = src.substring(src.lastIndexOf(".")+1)
    //   if (ext == 'svg') {
    //     loadSvg($(this), src, clss)
    //   }
    // })

  })

})

function loadSvg(svg, src, clss) {
  var svg_content = $.ajax({type: "GET", url: src, async: false}).responseText;
  console.log(svg_content)
  console.log(svg_content.responseText)
  svg.wrap('<div class="svg ' + clss + '" >'+ svg_content+'</div>')
}

function pageprint() {
  var divs = $("article");
  console.log(divs)
  for(var i = 0; i < divs.length; i+=2) {
    divs.slice(i, i+2).wrapAll("<div class='row'></div>");
  }
  window.print()
}
