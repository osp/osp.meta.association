# Register of members

## Board members

- Femke Snelting
- Eric Schrijver 
- Nik Gaffney
- Maxime Lambrecht


## Effective members

- Antoine Gelgon
- Gijs de Heij
- Pierre Huyghebaert
- Alexandre Leray
- Ludivine Loiseau
- Sarah Magnan
- Stéphanie Vilayphiou


## Source members

- Harrisson (Joël Vermot)
- Nicolas Malevé
- Pierre Marchand
- John Haltiwanger
- Colm O’Neill


## Adherent members

### Branch members

- Thomas Buxó  


### Observatory members 

- Fabien Dehasseler
- Greg Nijs
