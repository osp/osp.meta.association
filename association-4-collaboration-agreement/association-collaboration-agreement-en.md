title: Open Source Publishing
date: 2018-04-22 09:35
web-site: http://osp.kitchen/
slug: osp
categorie: collaboration-agreement
template: collaboration-agreement
cover: images/osp.svg
***

![](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/dear-you.svg)

***

![](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master//visual-language/img/efficiency-resiliency.svg)
We are writing you this letter to invite you to work together in a **specific way**. This text explains what could be this specificity, its backgrounds and sketches out its practical implications.

***


![](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/applied-research.svg){: .right}
It explains our ideals and how we pursue them through our **applied research practice**, be it on an aesthetics, on the relation between content and shape, on the tools we use, on the process.

***

![](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/user-student-teacher-developper.svg)
We think that digital technologies have and continue to **fully reshuffle every power relations**.


***


![](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/illich-eyes-conviviality.svg){: .right}
Every project faces these changing situations and contexts, asking for new practices. Every project then deserves to be thought and approached as an opportunity to experiment the displacement of practices and cultures as "tools of conviviality" ([Ivan Illich](http://www.mom.arq.ufmg.br/mom/arq_interface/3a_aula/illich_tools_for_conviviality.pdf), 1973). Why? Bluntly said: to rethink **how to live together**.

***

In order to achieve such singularities, we have experienced that **specific care is needed**, and implies a frame. We will build together this structural element partly for you to grasp how the project could follow a curved line and for us to tighten the curvature in a way that is sharable with you.

***

![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/parcours.png)
This letter will guide you through this bending operation and will show you how we can consider progressively more tools while journeying together!

***

# How tools and free software change our practice, and yours?

***


![](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/pancarte.svg)
By working with us, you know that we'll use *libre* and open source software. But practically what about you? It depends largely on the kind of project, but you can expect several slopes.

***

Most of the ingredients of design are based on pictures and texts. You will probably prepare them in a usual way for you and we'll be able to process them easily. We will propose you to use an open licence → see chapter [*“What about intellectual property?”*](#what-about) for the publication of these elements.

***

If it is not possible, i.e. for confidentiality reason when we work on [*Médor*](medor.coop) investigations, we will setup a specific route for the source files. For all other cases, they will be published on our website along with all design elements as we layout and develop the project. It implies that some elements of the project will be visible on our website during the work, so before the official release.

***

In our experience, it's quite vivifying and interesting to see the work evolve. If this is a problem for you for some reason, let's discuss it, there is probably a solution. And more importantly, it will probably tackle a potential issue to uncover before beginning to work.

***


![](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/clouds.svg)
To exchange big amount of files or huge ones, we use Nextcloud, a *libre* [cloud software](https://nextcloud.com/). When needed, you will be welcome on this web platform for exchanges, or even to install some dedicated client. The way it works is quite similar of other well-known cloud platform, and we will examine the small and interesting differences together.

***

If the project needs texts to be written together (i.e. notes), we will probably propose you to use [etherpads](http://etherpad.org/) and/or our own flavor of it. It is technically really easy, just a webpage to visit and we can start to co-write. But sometimes it could generate an unusual text editing promiscuity. We think that this new practice of co-authorship is interesting enough to go beyond our natural intimacy habits!

***

If the project involves a web interface, and is globally web-based, we will probably discuss with you about the different possible approaches. Web projects are hybrid design and software projects therefore follows complex development processes. We think that it is a media that needs to be questioned heavily. And even if it is quite recent, it is already difficult to avoid to follow unconsciously tracks which have been drawn in majority following industry injunctions.

***

More precisely, software is necessarily full of conventions often based on and reinforcing preexisting labor organizations. Mainstream web platforms tend to hide their digital materiality behind slick interfaces and in most cases makes it difficult for the user to see that materiality. This could give the false impression that software is “just” a vehicle, a transparent means for connecting creative ideas to the final output, and that the software has little to no effect on that output.

***

The culture of Free and *Libre* Software inverses the situation by putting the availability of the inner working of digital tools, their source code, at the center of its practice. We will enforce us to find ways that are more deeply rooted in our, —but also *your*— practices and contexts. This will probably bring us in uncommon areas. And it will also probably requestion classical closed-source social medias operations. Let's be aware of all this, and embrace fully the real emancipatory potential of these new media!

***

These are some common situations. We can expect various different ones with lots of resulting learning processes.

***

# How to define the research field?
![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/jachere.png)

***

As a field of creation, graphic design is inherently concerned with research, and a studio that wouldn’t claim for it would be surprising. We do also strongly embrace the research aspect, but consider research on a scope at a different scale. Research is about taking reasonable risks, exploring new territories, questioning our confidence and accepting to step out of our comfort zone.

***
If we accept together to confront with the awkwardness <<http://ospublish.constantvzw.org/blog/wp-content/uploads/awkward_gestures.pdf>> of going into some dose of unknown, it's a journey that we think could be more enriching for you and for us than only the final outcome. To use big words, it's an emancipation process, and it goes with some walking in the mist.

***


![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/going-together.png)
This question of the scale of the scope leads us to observe that a collaboration is not just a commission, neither just a job. From our previous experiences, we can address some points that can shift a job into a collaboration.
Initiating the project/process:

***
We decide together with you the outcome we want to reach.

![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/hill.png)
1. The outcome is a common goal and not a specific object.

2. This is done with a timeframe and a budget in mind.

***

More specifically, we agree together on when and how to stop.
![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/orientation-table.png)

***


![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/practice-redefine.png)
On the process/journey:
We valorise the trip, and not only the destination.

***

![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/milestone02.png)
Replacing “deadline” with “milestone”, and “estimate” with “roadmap” as good indicators.


![tiny](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/milestone01.png)
Practice redefines the outcome.
We gradually learn how our ideas about design can drive the instant need to meet deadlines.

***

Too often, requirements and specifications are preliminary to the job. Requirements include media output definition and technologies; or in other words “means”. Sometimes, the scope of those means is limited by underlying norms and/or previous experiences. Some other times, we fantasize about the perfect system that would fit them all without managing to grasp it.


![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/vague-ideas.png)
The first case tends to prevent us from engaging in a rich collaboration, where “means” are limiting the scope of imagination, as what we produce and how we produce are so closely intertwined. The second case tends to lead to vague ideas, unrealistic amount of work and in the end frustration when it comes to something that is only half-baked.

***

It is often hard to define a specific goal from the beginning, this is why part of the budget/calendar is dedicated to define altogether what this “something” is, building a shared vocabulary and a common understanding of what the project is and define its scope.

***

Concretely it means that part of the budget and calendar is dedicated to explore new territories without any guarantee of direct outcome at this stage of the project. After this first round of work, OSP and you decide whether to continue together or not. If the project is still on, OSP and you decide, based on the first round of research, on a specific outcome, which could even plan possible post-project developments.

***

More globally, we explore the design tension between politics and aesthetics.

![Politics and aesthetics](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/politics-aesthetics.png)

***

# How to plan the project?

***

We can very roughly summarize the timeline of a project.

- You come with a request
- We meet to redefine together your needs and how to address them → we have a collaboration
- We agree together on a calendar and budget
- OSP sets up a working team
- OSP makes one strong proposal with arguments
- We refine it together → we have a collaboration

***

- We work together
- You have a deliverable
- OSP shows you how to use our tools → your name appears next to ours in the project history
- You have new tools to work with
- OSP documents the process of the project
→ it invites other people to experiment with what we manipulated
→ you can be as close as you want to the project during the whole process
→ you are autonomous for the next steps, even without us
***

So based on your original request, we define together through a dialogue the objects to research and produce, the materials (text, images, video, audio…) to use, the potential collaboration with third-parties. We do not make three proposals and variations like many traditional graphic design companies as we think it is superficial and a time loss. After embracing the project, we prefer to focus all our time and energy into one strong proposal where ideas, visuals and tools work altogether. In the same move, we suggest you what tools will be used, especially the tools that you will manipulate directly.

***

# Who is the working team?

***

The tools and processes we practice do not coincide with individual experience, they move away from an ex-nihilo creative approach and find their energy in the collective. For reasons of sharing and exchange of skills with the sometimes experimental tools that we try and because thinking of a project with several people is always richer, we always work with a minimum of two people. Following this motto “never alone”, a team of two to four persons is set up for each project according to one’s interests and agendas.

***

## Conscience

***

Represented by one or two people, the conscience is the middle ground between you and the rest of the team. Being the relay does not mean being responsible for the rest of the team. The conscience is often the person through whom the project came in, or is chosen by affinities with the project, or simply for internal logistics.

***

## External conscience

***

We try to set up the status of an external conscience, an OSP member who is not part of the working team. S·he has the role of forester, watching, browsing the milestones of the project as an external eye/advisor, helping with tough decisions, general management, planning or logistics of the research project.



***



These two roles are useful for our ideas of collaborations but not applicable to all projects and could be redeveloped with you. The main idea is to be attentive together to the evolution of the project to keep a good momentum at each step and not bury too long in working tunnels without keeping an overview of the project

***

![large](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/documentation-iceberg.svg)
# Why documentation is important?

***

We inherit a set of thoughts and habits from the tools we choose to use. Source sharing and publishing are values we hold strong. This is a model that we borrowed from the Free Software culture, based on how rich, diverse and collaborative it has become. During a project, the working team works together using a versioning system which houses the files for the project. This repository is accessible directly on OSP website for the collaborator, and also for the outside public. Not only does this enable us to publish sources, but it also keeps track of how the project has progressed over time and who has contributed to it.

***

A good documentation is important because it determines the power to become autonomous for the collaborators, and all future users. For example, if a cooking recipe do not describe all the process of making and optimizing, the user will have lots of difficulties to really drive the project to a fruitful end.

![large](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/intro-documentation.svg)
***

The documentation will be composed of:
- the folders and files of the project while we are working on it
- the history of the project: messages describing what/why/how we changed the files
- an «iceberg» folder, a folder where we put screenshots of the project during the whole process. This is intended to show trials, paths put aside but which are nonetheless interesting for another future project
- a «showcase» folder where we put screenshots, pictures of the final project to make it more visible to the visitors of our website
- the «README» file where we narrate with words and pictures the project: what we used and why (tools, typefaces, shapes, cultural references…)

***

The idea behind this documentation is more to show a process and recipes rather than giving ready-to-use materials and tools (which can also happen) in order to invite people to reappropriate what we do rather than merely re-using it.

***

> Free Software does not explain why these various changes have occurred, but rather how individuals and groups are responding: by creating new things, new practices, and new forms of life. What is most significant are these practices and forms of life —not the software itself, and they have in turn served as templates that others can use and transform: practices of sharing source code, conceptualizing openness, writing copyright (and copyleft) licenses, coordinating collaboration, and proselytizing for all of the above.
>
> <footer>Chris Kelty, <i>Two Bits: The Cultural Significance of Free Software</i></footer>

***

# What about the budget?

***

At the start of the project, we setup a quote and decide together with you of the installment payments. As a default proposal, we ask for 25% of the budget within a month after the start of the project. Another 25% at the middle of the project calendar. And the 50% leftover at the end of the project. For quick projects, we ask for 50% of the budget within a month after the start of the project.

***

These installments are important to avoid the common tenuousness and precarity in graphic design. Also, 25% of each budget is dedicated to OSP asbl/vzw to cover fixed costs.

***

# What about intellectual property?

***

> The representational freedom of artists, part of which is the freedom to depict and build or comment on existing culture, to continue the conversation of culture, is the freedom of art.
>
> <footer>Rob Myers, <i>Open Source Art Again</i>, 2006</footer>

***

Some of the concepts described above come from the free software freedom and duties, also known as copyleft:
![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/copyleft.svg)
​    

***

- Freedom 0: to use the work,
![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/freedom0.png)

***

- Freedom 1: to study the work,
![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/freedom1.png)

***

- Freedom 2: to copy and share the work with others,
![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/freedom2.png)

***

- Freedom 3: to modify the work, and the freedom to distribute modified and therefore derivative works.
![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/freedom3.png)

***

- Duty 1: to attribute the work and its parent versions (the copyright)
![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/freedom4.png)

***

- Duty 2: to distribute any derivative work under the same or equivalent license.
![small](https://gitlab.constantvzw.org/osp/osp.meta.association/raw/master/visual-language/img/freedom5.png)

***

[//]: # "VISUAL 4 JAMS - http://blog.osp.kitchen/news/the-four-freedoms-two-rules-and-one-jam.html"

***

Free and Open Source License are a “hack” on the traditional copyright system. Because the copyright owner determines who has permission to use the material, the copyright owner can also decide if and how to open it up. To be able to license the designs under a Free and Open Source license, OSP retains the copyright to their designs.

***

In the joint creation of the design we will be using all kinds of visual and textual materials provided by you or by your partners. In the spirit of OSP, we urge you to use your intellectual property to open up this material under the same copyleft licenses OSP uses, and to convince your partners to do so as well.

***

The goal of these licenses is that the whole of the project files, both its process and its final result, become available for the community to build upon. This does not mean that strangers will be able to change directly the design we make for you. You and OSP remain responsible for the design we think together. It rather enables others to take elements of our work and use it as inspiration for theirs.

***

We are well aware that it is not always possible to use a license that enables re-use in a modified form. This could be the case, for example, with the logos of supporting organizations, photographs made by a third party… In that case we require at least the right to redistribute these images with a notice specifying the usage rights of those files; in this way the selection of files that makes up a project can still be shared as a whole, even if the usage requirements of the discrete elements differ.

***

# Why sharing sources?

***

Free software challenges traditional economical paradigms because professionals and amateurs are part of the same ecosystem, and instead of creating scarcity, embrace the possibility that one’s work can be copied.

***

As we have see, the licenses OSP uses on its production are copyleft. It means that anyone is free to reuse, modify and redistribute our materials, even for commercial purposes. That new flavour of the material has to be, however, redistributed under the same license: the ecology of sharing is therefore stimulated and preserved. It doesn’t mean we give up our authorship, but that we invite others to get influenced by others and to acknowledge this.

***

It may sound unusual —even scary— in a society that overvalues the outcome over the process. We don’t mind sharing source as we consider the value of our work to be the creative path that leads to an outcome (*Poiesis*), and not just the outcome itself (*Aesthesis*). In other words, we like to think of our work in terms of practice and not production.

***

Furthermore we think of design as a space for dialogue and tension between cultures. We see all the parts in the process of  producing design to be parts of culture, from the tools to the recipes and the final object and therefore should them be free to use by all.

***

Some edge cases might prevent us from publishing right away the sources though. In journalism for instance, secrecy is not only an economical issue but also a requirement: one might need to strategically avoid immediate publishing or even literally protect the sources. The working team and the collaborator agree at the beginning of the project when and what sources are to be published. We currently not share accounting elements because of the way it is strongly related to the capitalist rules which bend the society in one main direction. But this current decision could change in the future following our experience.

***

But when the project is published, it is important to OSP to publish not only the resulting files but also their history, the narration of the project. It is not enough to declare the freedom of an object for this object to be freed. A necessary load of pedagogy that pass through documentation is needed.

***

<!-- [VISUAL] "Le corbeau calédonien n'est pas un oiseau comme les autres : il fait partie des rares animaux qui utilisent des outils. Il fabrique par exemple des sortes "d'hameçons à larves" avec des branches, lui permettant d'attraper ses proies dans des trous ou fissures. Mieux encore, il sait mettre de côté les outils qui lui ont été utiles, afin de les réutiliser plus tard, ce qui dénote un sens de l'anticipation.
Mieux encore, il se pourrait que ce corvidé ait une certaine capacité à montrer à ses congénères comment fabriquer un outil, un degré de transmission qui n'est jusqu'ici connu que chez les humains, qui passent leur technologie de génération en génération tout en l'améliorant : ce que les spécialistes nomment la "culture technologique cumulative". Cela serait-il possible chez ces oiseaux ?" -->

***

# Who is OSP?
![](../visual-language/img/osp-frog.svg)

***

OSP is a group of practitioners centered around graphic design using only Free and Open Source Software — pieces of software that invite their users to take part in their elaboration. Sometimes, OSP makes its own software, but it is always inscribed in an ecology of existing software. All materials produced are under a free license, acknowledging that —in software, in design, in art— nothing is new, everything is remixed.

***

We are interested in graphic design as a space of tension/dialogue between public, culture, etc.  We try to adopt an ecological approach. To do so, by re-inventing the conditions of our practices: task-separation, workflows, education and power struggles.

***

OSP takes part in elaborating software in many ways: showcasing the software by using it, inviting to use it by documenting our process, participating in its elaboration by giving feedback, asking for features or modifying it.

***

<!-- [VISUAL]: anonymous sentences about why we are in osp, what we are looking for, in bubbles, Venn diagram <https://en.wikipedia.org/wiki/Venn_diagram>?-->

***

Read more about our philosophy in our text [«Relearn»](http://osp.kitchen/api/osp.writing.relearn/5e0cdc51d618e150a67af0f18c21823afa17fa28/blob-data/EN_17-04-18-book.pdf).

***

# How our collaboration is legally structured?

***

Currently, OSP is a bilingual Belgian non-profit organization (asbl/vzw).

***

Maybe one day OSP will setup a cooperative for its commissioned works and keep the non-profit association for its pedagogical and self-initiated activities. The status of a cooperative reflects better than a regular enterprise the fact that OSP wants to keep a research activity in its commissioned works.

***

It also reflects the facts that OSP is not seeking for personal enrichment but that OSP wants to make a framework that is sustainable. But currently, cooperatives are also meant to make profit, to build a capital, and we are not all fully comfortable with this notion.

***

If you read this letter when wanting to start a project with us, and if you agree with it, sign it and return it to us. Along with an agreed quote, it will give insurance for you and for us that we are on a common and fruitful ground.



***

Best consideration,
Open Source Publishing asbl  
miam@osp.kitchen  
<http://osp.kitchen>


This document is still in progress. Follow the [working repository](https://gitlab.constantvzw.org/osp/osp.meta.association/tree/master/association-4-collaboration-agreement)



<script>
    var customPageWidth = "210mm";
    var customPageHeight = "297mm";
    var customPageNumber = 18;
    var crop = "0";
</script>



<!-- <style media=all>
    img {
        /*float: left;*/
        margin: 0 25%;
        float: none !important;
        clear: both !important;
    }
    img.right {
        /*float: right;*/
    }
    img[alt="small"] {
        max-width: 2.5cm !important;
        margin: 0 35%;
        }
    img[alt="tiny"] {
        max-width: 1.5cm !important;
        margin: 0 35%;
        }

</style> -->
