<!--
- Loi ASBL : <http://www.ejustice.just.fgov.be/cgi_loi/change_lg.pl?language=fr&la=F&table_name=loi&cn=1921062701>
- <http://www.ejustice.just.fgov.be/cgi_loi/change_lg.pl?language=nl&la=N&cn=1921062701&table_name=wet>
- Statuts ASBL Parlement Jeunesse : <http://www.ejustice.just.fgov.be/tsv_pdf/2011/11/23/11176583.pdf>
-->


# Open Source Publishing ASBL <br />Statutes

The undersigned:

- Gijs de Heij – NL – rue des Tanneurs 170, 1000 Bruxelles
- Pierre Huyghebaert – BE – rue Cervantes 6, 1190 Forest
- Alexandre Leray– FR – rue de l’Hôtel des Monnaies 27, 1060&nbsp;Saint‑Gilles
- Ludivine Loiseau – FR – rue Defacqz 13, 1050 Ixelles
- Sarah Magnan – FR – rue Franz Merjay 93, 1050 Ixelles
- Colm O’Neill – IE – avenue Félix 6, 1330 Rixensart
- Eric Schrijver – NL – Alfred Cluysenaarstraat 12, 1060 Saint-Gilles
- Stéphanie Vilayphiou – FR – rue de l’Hôtel des Monnaies 27, 1060&nbsp;Saint-Gilles

have agreed on constituting a non-profit association in accordance with the law of the June 27, 1921, adapted and modified by the law of  May 2, 2002, which statutes are as such:





## Title I : Denomination, registered office, goal, period


### Article 1: Denomination

The association is named “Open Source Publishing”, abbreviated into “OSP”. This name has to figure in all acts, bills, notices, publications, letters, order forms and all other documents coming from the association, and has to be immediately preceded or followed by the terms “non-profit association” or the abbreviation “asbl” or “vzw”, with the precise indication of the address of the registered office of the association.


### Article 2 : Registered office

Its registered office is established at WTC, tour 1, Boulevard du Roi Albert II 30, 1000 Bruxelles in the judicial district of Brussels.

Any modifications of the registered office is of the exclusive competence of the general assembly which will vote on this subject, in accordance with the law of June 27, 1921, adapted and modified by the law of May 2, 2002.


### Article 3 : Goal, social object, field of action

The association OSP has as its main goal to propagate Free and Open Source culture in Brussels and internationally. More specifically, OSP asbl aims to stimulate the social movements of Free Culture and Free Software in the field of graphic design.

Free Culture and Free Software challenge the excesses of copyright and one of the ideas which underly copyright: the ideas of originality and of artistic creation as a solitary act. Instead they propose a vision of artistic creation which includes collaboration, exchange and creative re-appropriation.

Free Culture chooses not to reject copyright outright, but to appropriate and subvert it with its own means (also known as “copyfight”). Participants in Free Culture choose to distribute artistic works under a license which permits others many of the freedoms normally lacking in traditional copyright licenses: the freedom to freely use the works, modify them and redistribute the modified copies.

Free Software as a concept applies to computer software; an example of a Free Software license is the GNU General Public License. Free Culture as a concept applies to other cultural works such as designs, drawings, compositions, texts. An example of a Free Culture license is the Free Art License and  the Creative Commons Attribution Share-Alike license.

Free Culture and Free Software are part of what is more popularly known as “Open Source”. More specifically Free Culture and Free Software are “share-alike”: that is to say, a work of Free Culture and of Free Software can only be used by others if the resulting work is also licensed as Free. They can thus be seen as to be concerned with creating an alternative ecology which features a different way of dealing with intellectual property; an ecosystem that can exist next to but cannot easily be fully appropriated by the existing privative regimes of intellectual property.

While one can make Free Culture with proprietary software, and one can use Free Software to produce creative works under traditional copyright licenses, OSP asbl stimulates the use of Free Software to create Free Culture in order to develop a coherent ecosystem of freedom. Indeed Free Software, by its open nature, invites to understand the mechanisms of digital tools to then manipulate them in a critical and constructive way. Practices shape tools—tools shape practices.

OSP asbl pursues the realization of its goal by all means but more specifically through three axis: pedagogy, research and graphic design. OSP asbl gives workshops in the frame of traditional educational institutions and organizes alternative events outside of that frame which further question how Free Culture can change existing pedagogy. In collaboration with cultural or educational institutions, OSP asbl is able both to develop a theoretical framework and the open tools which exemplify this position. By engaging in graphic design practice, both as part of its own projects and those of others, the hypotheses from the research are tested in practice, and new questions arise. Finally, editing and publishing are a means to serve OSP asbl goal, as are public events: print parties, workshops, performances, exhibitions and lectures.


### Article 4 : Duration of the association

The association is constituted for an undetermined period. It can be dissolved at any time.





## Title II - Members


### Article 5 : Members

Different categories of members are defined in the Recipe of Interior Order (ROI), with different voting rights.


### Article 6 : Role of the steward and reports

A steward role is established. The steward makes the connection between the members and board. He/she is elected from the effective members following a rhythm and criterias precised in the Recipe of Interior Order. The steward is responsible to report effective members activities at least once a quarter. This report can be sent via mail or be a meeting with one or more board members.


### Article 7 Admission – departure – suspension – exclusion of members and members deemed to have resigned

Any candidate to the different statutes of members, described in the ROI, has to follow the according procedure.

Any member is free to disengage with the association by writing the dismissal by email to the administrative board.

Is deemed to have resigned the member who doesn’t assist or does not get represented to three consecutive general assemblies if his/her presence is inquired by the ROI.

The exclusion of a member can be pronounced only by the general assembly to the majority of two thirds of the present or represented voices. The administrative board can suspend, until the decision of the general assembly, the members who would be guilty of a serious violation of the statutes or of the laws.

The exclusion of a member requires the following conditions:
1. The mention in the general assembly agenda of the exclusion proposal mentioning, even briefly, the reason of such a proposal.
2. The regular meeting of a general assembly where all the effective members should be called.
3. The decision of the general assembly has to be validated on the 2/3 of the voices of the voting members present or represented.
4. The respect of the defense rights, meaning the hearing of the member whose exclusion is asked, if the latter is willing to.
5. The mention in the register of the exclusion of the member.

Being a decision concerning a person, this should necessarily be taken through secret vote.

The quality of member is automatically lost with the death of the member or, if it concerns a moral person, with its dissolution, its fusion, its split, its nullity or its bankruptcy.

The resigning member, suspended or excluded, together with his/her heirs or right-holders of the passed member, have no right on the social fund of the association. They cannot claim or require nor bank statements or yielding, nor the apposition of seals, nor inventories, nor the refund of paid membership fees if any.


### Article 8 : Register of members 

The association has to maintain a register of members under the responsability of the administrative board.

Any decision of admission, departure or exclusion of members is inscribed into the register at the instance of the administrative board maximum fifteen days after when the board has known of the changes made.

Any member can consult the register of members, together with all the minutes and decisions of the general assembly, the administrative board, and also any accounting document of the association, upon request by email, motivated and addressed to all the members of the administrative board. The members must precise which documents they want to consult. The administrative board must send the asked documents within a month.





## Title III - Membership fees

### Article 9 : Membership fees

Members can be submitted to an entrance fee and/or a subscription fee. The amount for each type of member is precised in the ROI.





## Title IV - General Assembly

### Article 10 : Composition

The general assembly is composed of all effective members, source members and board members. Only these categories have a voting right. The adherent members can take part in the general assemblies but have no voting right.

The board designates one of his members to preside each general assembly.


### Article 11 : Admission

Can become effective members the adherent members showing an active and regular practice within the association. They can then apply or be invited by one or several effective members. Their entrance as an effective member is voted by the effective members during one of their regular meetings and is approved by a majority of 3/4 of effective or source members present or represented. The admission or the rejection is definitive and has not to be motivated by the effective members. It is communicated to the candidate by email.


### Article 12 : Powers

The General Assembly is the sovereign power of the association. It is in particular responsible for:

- Amendment of the statutes;
- The exclusion of members;
- Appointment and removal of administrators, the auditors and the liquidator;
- The fixing of the remuneration of auditors in cases where compensation is allocated;
- The approval of accounts and budgets;
- Discharge to annual awards to administrators, to auditors, to liquidators;
- The voluntary dissolution of the association;
- The possible transformation into a social purpose company;
- The decision of the destination of the assets upon dissolution of the association;
- All cases required in the articles;
- Decide to take legal action for damages against any member of the association, any administrator, any auditor, any person authorized to represent the association or any agent appointed by the general assembly.
  
<!--
- Approval of the rules of recipe and amendments; → nb: this one disappeared from the FR version
-->


### Article 13 : Convocation – Ordinary general assembly


The effective and source members are summoned to the ordinary general assembly which happens at least once a year.

The general meeting is convened by the administrator appointed by the board via email at least fifteen days before the date. The notice shall specify the date, time, place and agenda.


### Article 14 : Délibération

The general assembly can take place when half of its members with voting power are present or represented, except in cases where the law requires attendance quorum and a quorum of the vote, the statutory modification, exclusion of a member, the dissolution of the association or the transformation into a company with social purpose.

The general assembly can only may validly deliberate on the items on the agenda.

The general meeting shall be called by the board when fifth of the members on the written request. Similarly, any proposal signed by three effective or source members must be brought to the agenda of the next general meeting.

Decisions are made by simple majority of the votes of present members, except in the case where it is decided otherwise by the law or by these statutes.
In the case of equal amount of votes for and against, the vote from the president of the general assembly is overriding.


### Article 15 : Representation

Any member can be represented by another member as long as the former gives procuration either written or by email. One member can hold only one procuration.


### Article 16 : Statutes Modifications and dissolutions

The general assembly can only validly deliberate on the statutes modifications only if those modifications are explicitely specified in the convocation and if the assembly gathers at least two thirds of the effective members, whether present or represented.

No modifications can be adopted but with the majority of 2/3 of the voices of present or represented members. However the modification concerning the goal of the organization can only be adopted with the majority of 4/5 of the present or represented members. If 2/3 of the effective members are not present nor represented at the first meeting, a second meeting can be set up which could then deliberate validly, regardless of the number or present or represented members, and adopt the modfications to the majorities intended at paragraph 2 or at paragraph 3. The second meeting cannot be held less than fifteen days after the first meeting.

Any modification of the statutes or decision relative to the dissulution must be deposited, whithin a month, to the greffe du tribunal of commerce for publication as an annexes to the Moniteur belge. Similarly for any nomination or cessation of activity for an administrator, a person able to represent the association, a daily management delegate or a financial delegate.


### Article 17 : Publication of the decisions voted by the general assembly

Summons and minutes that contain general assembly decisions, along with accounting documents, must be signed by an administrator. They are to be kept in a registrer and can be viewed by any member or third party as long a valid reason is provided, and approved by the board.





## Title V - Board


### Article 18 : Mandate

The board of the organization is compound of 3 members at least and 5 at most, nominated and revocable by the general assembly.

The duration of the mandate is illimited. The mandate expires by death, resignation or revocation.


### Article 19 : Election of the board

An election to an unallocated board post takes place on the proposal of the board, or when a third of the members requests it.

Anyone is eligible to be a board member of the organization. The outgoing board members are reeligible.

The applications for the board must arrive by email and be signed by at least three effective members after having physically met or by phone or other telematic means or any way of getting a feel for the texture of the voice of the candidate. This must be done at least 7 days before the general assembly. The sum of administrators will always remain inferior to the amount of effective members of the general assembly.

The election of the board members are made tenure by tenure.

The election is validated by an absolute majority of the votes cast. If no candidate gets the absolute majority, an additional voting round is organized between the two candidates who got the most votes in the first round. Candidates in a tie vote for second place are all represented in the additional round, and we proceed as such until an absolute majority is obtained.


### Article 20 : Resignation

Any administrator who wishes to resign must make his decision known by email to the board.


### Article 21 : Frequency of board meetings

The board meets whenever there is a need for a meeting. It is summoned when at least three administrators make the request. Board meetings are presided by one administrator appointed before each meeting.


### Article 22 : Deliberation

The board deliberates validly as long as at least half of its members are present or have correct representation. Their decisions are made by simple majority of the present or represented votes.

In the case of equal amount of votes for and against, the point is carried over to the next board meeting.


### Article 23 : Powers

The board has the most extended powers for the administration and general managing of the association. The board works upon a collegiate principle. All attributions that are not expressively reserved by law or by these statutes to the general assembly are exercised by the board.

It can, non exclusively, approve acts and contracts, open and manage bank accounts, deal, compromise, acquire, exchange or sell any goods, material or immaterial, mortgage, borrow, deal leases, accept legacy, subsidy, donation and transfer, refuse any rights to represent the association in court, both as defendant or prosecutor. It can also name and refuse the staff of the association.

The board controls the modifications of the Recipe of Internal Order made by the effective members.


### Article 24 : Delegation to the day-to-day management

The board can delegate some of its powers to one or more people, administrators or not, acting collaboratively.

The powers of the daily management body are limited to daily management tasks which involve:

- no task going beyond the needs of the daily activities of the association
- no task which, with regards to their low level of importance and the necessity of a promt solution do not justify the intervention of the board.
- The lenght of the mandate of these management delegations, eventually renewable, is to be determined by the board. When the management delegate also holds an administrative role, the end of the administators mandate directly includes the end of the management delegation.

The board can, at any moment and without necessary justification, end the functions of a management delegate.


### Article 25 : Representation

The association can validly be represented in court by at least two administrators, appointed by the board, acting collaboratively and who, as a body, will not be required to justify to a third-party any earlier decision and any procuration of the board.





## General assembly of founding members

The founding members, reunited as a general assembly on February, 23rd of 2015, have designated as their first board members:

- Bram Crevits – BE – Paul Frederiqstraat 80, 9000 Gent
- Nik Gaffney – AU –  rue des Minimes 60, 1000 Bruxelles
- Catherine Lenoble – FR – rue de Savoie 85, 1060 Saint-Gilles
- Femke Snelting – NL – avenue Julien Hanssens 42, 1080&nbsp;Molenbeek‑Saint‑Jean
- Maxime Lambrecht – BE – avenue de la Couronne 271, bte 2, 1050&nbsp;Ixelles

